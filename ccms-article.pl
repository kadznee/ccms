#!/usr/bin/perl

# Program: small cms - article builder
# Author: Khaerul Adzany <khaerul (dot) adzany (at) gmail (dot) com

use File::stat;
use Time::localtime;
require "./util.pl";

$out_article = "<h1>Berita</h1>";
$single_article = "";
$temp_article = "";
$temp_content = "";
$temp_tmpl = "";
$temp_link = "";
$file_date_string = "";
$file_to_write  =  "security_news.content";

# Open the index template
open(TEMPLATE, "template/index.tmpl");
while($template_text = <TEMPLATE>) {
	$temp_tmpl .= $template_text;
}
close(TEMPLATE);

# Get the articles, write to news
opendir(ARTICLEDIR, "./content/articles") || die "Cannot open ./content/article directory, please check if directory exists or accessible.";
@article_array = grep {/[.]article$/} readdir(ARTICLEDIR);
closedir(ARTICLEDIR);

foreach $article(@article_array) {
	# Initializes values
	$file_date_string = ctime(stat("./content/articles/$article")->mtime) or die "Fail to read article data.";
	$temp_content = "";
	$temp_article = "";
	
	open(ARTICLECONTENT, "./content/articles/$article");
	while($text_in_content = <ARTICLECONTENT>)
	{
		$temp_content .= $text_in_content;
	}
	close(ARTICLECONTENT);
	
	$article =~ s/.article$//;
	$temp_link  = $article;
	$temp_article .= "<div class=\"article\"><div class=\"article_head\"><h2><a href=\"$temp_link.html\">";
	$article =~ s/_/ /g;
	$temp_article .= "$article</a></h2>";
	$temp_article .= "<span>$file_date_string</span></div>";	
	$temp_article .= $temp_content;
	$temp_article .= "</div>";
	
	# Copy the single article
	$out_article .= str_replace_stop_at_more("[more]", "<a href=\"$temp_link.html\">... (more)</a></div>", $temp_article);
		
	open(SINGLEHTMLFILE, ">html/$temp_link.html");
	print SINGLEHTMLFILE str_replace_global("[security_news]", "<h1>Berita</h1>".$temp_article, $temp_tmpl);
	close(SINGLEHTMLFILE);
}

# Create the tmpl file, and write into it
open(TMPLFILE, ">content/$file_to_write");
print TMPLFILE $out_article;
close(TMPLFILE);

__END__