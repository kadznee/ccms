#!/usr/bin/perl

# Program: small cms - entry 
# Author: Khaerul Adzany <khaerul (dot) adzany (at) gmail (dot) com

# TODO:
# =====
# + Copy images on folder ./template/images to ./html/images
# + Improve the menu-building method

use File::Copy;
use File::stat;
use Time::localtime;
require "./util.pl";

# Get the current template name to be written
$template_name = $ARGV[0];
$template_title = $ARGV[1];
$menu = "";
$security_feature = "";
$security_news = "";
$temp = "";
$temp_tmpl = "";
$temp_result = "";

# Special, for news section
$out_article = "<h1>Berita</h1>";
$single_article = "";
$temp_article = "";
$temp_content = "";
$temp_link = "";
$file_date_string = "";

# Check whether the input param is correct
unless($template_name =~ /[.]tmpl$/){
	die "Template must be in *.tmpl format!\n";
}

if($template_title =~ //){
	$template_title = $template_name;
}

unless(-e "template/".$template_name) {
	die "template file: " . $template_name ." not exists\n";
}

# Copy the style file
$source = "./template/stylesheet.css";
$target = "./html/stylesheet.css";
copy($source, $target) or die "stylesheet.css does not exists.";

# Open the specified template
open(TEMPLATE, "template/".$template_name);
while($template_text = <TEMPLATE>) {
	$temp_tmpl .= $template_text;
}
close(TEMPLATE);

# Set the page title here
$temp_result .= str_replace_found_first("[title]", $template_title, $temp_tmpl);

# Get the templates for creating menu
opendir(TMPLDIR, "template") || die "Cannot open template directory, please check if directory exists or accessible.";
@tmpl_array = grep {/[.]tmpl$/} readdir(TMPLDIR);
closedir(TMPLDIR);

if($template_name =~ /index.tmpl/) {
		$menu .=  "<li><a class=\"current\" href=\"index.html\">index</a></li>";
}
else {
		$menu .=  "<li><a href=\"index.html\">index</a></li>";
}

foreach $template(@tmpl_array) {
	unless($template =~ /index.tmpl/) {
		if($template =~ $template_name) {
			$template =~ s/.tmpl//;
			$menu .=  "<li><a class=\"current\" href=\"$template.html\">$template</a></li>";
		}
		else {
			$template =~ s/.tmpl//;
			$menu .=  "<li><a href=\"$template.html\">$template</a></li>";
		}
	}
}

$temp_result = str_replace_found_first("[menu]", $menu, $temp_result);

# Create contents based on template
opendir(CONTENTDIR,  "content") || die "Cannot open content directory, please check if directory exists or accessible.";
@content_array = grep {/[.]content$/} readdir(CONTENTDIR);
closedir(CONTENTDIR);

foreach $content(@content_array){
	# Get the templates
	$temp = "";
	open(CONTENT, "content/".$content);
	while($text_in_content = <CONTENT>)
	{
		$temp .= $text_in_content;
	}
	close(CONTENT);
	$content =~ s/.content//;
	$temp_result = str_replace_found_first("[".$content."]", $temp, $temp_result);
}

# Get the articles, write to news
opendir(ARTICLEDIR, "./content/articles") || die "Cannot open ./content/article directory, please check if directory exists or accessible.";
@article_array = grep {/[.]article$/} readdir(ARTICLEDIR);
closedir(ARTICLEDIR);

foreach $article(@article_array) {
	# Initialize values
	$file_date_string = ctime(stat("./content/articles/$article")->mtime) or die "Fail to read article data.";
	$temp_content = "";
	$temp_article = "";
	
	open(ARTICLECONTENT, "./content/articles/$article");
	while($text_in_article = <ARTICLECONTENT>)
	{
		$temp_content .= $text_in_article;
	}
	close(ARTICLECONTENT);
	
	$article =~ s/.article$//;
	$temp_link  = $article;
	$temp_article .= "<div class=\"article\"><div class=\"article_head\"><h2><a href=\"$temp_link.html\">";
	$article =~ s/_/ /g;
	$temp_article .= "$article</a></h2>";
	$temp_article .= "<span>$file_date_string</span></div>";	
	$temp_article .= $temp_content;
	$temp_article .= "</div>";
	
	# Copy the single article
	$out_article .= str_replace_stop_at_more("[more]", "<a href=\"$temp_link.html\"> selengkapnya... </a></div>", $temp_article);
	
	$single_article = str_replace_global("[security_news]", "<h1>Berita</h1>".str_replace_global("[more]", "", $temp_article), $temp_result);
	open(SINGLEHTMLFILE, ">html/$temp_link.html");
	print SINGLEHTMLFILE $single_article;
	close(SINGLEHTMLFILE);
}

# Create the final html file, and write into it
$template_name =~ s/.tmpl//; 
open(HTMLFILE, ">html/$template_name.html");
print HTMLFILE str_replace_found_first("[security_news]", $out_article, $temp_result);
close(HTMLFILE);

__END__

