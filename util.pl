#!/usr/bin/perl

# Sub: str_replace() - String Replace without RegExp
# Usage: string str_replace ( string search, string replace, string subject );
# Origin: http://www.bin-co.com/perl/scripts/str_replace.php\
sub str_replace_found_first {
    my $replace_this = shift;
    my $with_this  = shift; 
	my $string   = shift;
	
	my $length = length($string);
	my $target = length($replace_this);
	
	for(my $i=0; $i<$length - $target + 1; $i++) {
		if(substr($string,$i,$target) eq $replace_this) {
			$string = substr($string,0,$i) . $with_this . substr($string,$i+$target);
			return $string;
		}
    }
	return $string;
}


# Sub: str_replace_global() - String Replace without RegExp
# Usage: string str_replace_global ( string search, string replace, string subject );
# Origin: http://www.bin-co.com/perl/scripts/str_replace.php\
sub str_replace_global { 
	my $replace_this = shift;
	my $with_this  = shift; 
	my $string   = shift;
     
	my $length = length($string);
	my $target = length($replace_this);

	for(my $i=0; $i<$length - $target + 1; $i++) {
		if(substr($string,$i,$target) eq $replace_this) {
			$string = substr($string,0,$i) . $with_this . substr($string,$i+$target);
		}
	}
	return $string;
}

# Sub: str_replace_stop_at_more() - String Replace without RegExp
sub str_replace_stop_at_more { 
	my $replace_this = shift;
	my $with_this  = shift; 
	my $string   = shift;
	
	my $length = length($string);
	my $target = length($replace_this);
	
	for(my $i=0; $i<$length - $target + 1; $i++) {
		if(substr($string,$i,$target) eq $replace_this) {
			$string = substr($string,0,$i) . $with_this; #should stop here
			return $string;
		}
	}
	return $string;
}

1;
